import logging
import os

from google.appengine.api import app_identity
from apiclient.http import MediaFileUpload

from src.framework.request_handler import BaseRequestHandler, jinja2
from src.framework.api import service, decorator, client, driveService

# import our models
import src.models as models

#import google users api
from google.appengine.api import users

class SubmissionHandler(BaseRequestHandler):
    def get(self, courseId):
        upload_url ='/content/submission/' + courseId

        template_parms = {
            'courseId' : courseId,
            'uploadUrl': upload_url
        }

        self.render('content/submission.html', **template_parms)

    @decorator.oauth_required
    def post(self, courseId):
        try:
            self.response.out.write(courseId)

            id = courseId
            # id = jinja2.escape(id)
            assTitle = self.request.POST.get('assTitle')
            assTitle = jinja2.escape(assTitle)
            assDescription = self.request.POST.get('assDescription')
            assDescription = jinja2.escape(assDescription)
            fileUrl = self.request.POST.get('fileUrl')
            fileUrl = jinja2.escape(fileUrl)

            course_work = {
                'title': assTitle,
                'description': assDescription,
                'workType': 'ASSIGNMENT',
                'state': "PUBLISHED",
                'materials': { 'link': { 'url': fileUrl }}
            }
            self.response.out.write(course_work)

            course_work = service.courses().courseWork().create(courseId=id, body=course_work).execute(http=decorator.http())

            self.response.out.write(course_work)

            # # get current user
            # user = users.get_current_user()
            # # get id of user
            # ownerId = user.user_id()
            # email = user.email()
            #
            # # create announcement object from our models
            # announcement = models.Announcement()
            #
            # # pass data to announcemnt object
            # announcement.title = assTitle
            # announcement.content = assDescription
            # announcement.courseId = id
            # announcement.ownerId = ownerId
            # announcement.ownerEmail = email
            #
            # # write to db
            # announcement.put()
            # self.response.out.write(announcement)

            self.redirect('/course/%s' % courseId)


        except Exception as e:
            self.response.out.write(e)

class StudentSubmissionHandler(BaseRequestHandler):

    @decorator.oauth_required
    def get(self, courseID, courseWorkID):

        # get submissions for this courseWork id
        submission_results = service.courses().courseWork().studentSubmissions().list(courseId=courseID, courseWorkId=courseWorkID).execute(decorator.http())

        courseWork = service.courses().courseWork().get(id=courseWorkID, courseId=courseID).execute(decorator.http())
        
        course = service.courses().get(id=courseID).execute(decorator.http())

        submissions = self.getKeyFromData(submission_results, 'studentSubmissions')


        template_parms = {
            'title': "Submissions",
            'submissions': submissions,
            "course": course,
            'courseWork': courseWork
        }

        self.render('submission/submissions.html', **template_parms)
